function(power base exponent)
    set(POWER 1)
    if (${exponent} GREATER 0)
        math(EXPR exponent "${exponent} - 1")
        foreach(l RANGE ${exponent})
            math(EXPR POWER "${POWER} * ${base}")
        endforeach()
    endif ()
    set(POWER ${POWER} PARENT_SCOPE)
endfunction()


add_custom_target(jacobi)
add_custom_target(nn)
add_custom_target(preconditioner DEPENDS nn jacobi)


foreach(REFINEMENT_LEVEL RANGE 1 7)
    power(2 ${REFINEMENT_LEVEL})
    set(NBLOCKS ${POWER})

    foreach(TYPE jacobi nn)
        if(NBLOCKS GREATER_EQUAL 2)
            set(DO_SIMD 1)
        else()
            set(DO_SIMD 0)
        endif()

        set(target ${TYPE}_block-${NBLOCKS})
        configure_file(${TYPE}.ini ${CMAKE_CURRENT_BINARY_DIR}/${target}.ini)
        configure_file(${TYPE}_driver.cc ${CMAKE_CURRENT_BINARY_DIR}/${target}_driver.cc)

        dune_add_generated_executable(
                UFLFILE poisson.ufl
                TARGET ${target}
                INIFILE ${CMAKE_CURRENT_BINARY_DIR}/${target}.ini
                SOURCE ${CMAKE_CURRENT_BINARY_DIR}/${target}_driver.cc
        )
        add_dependencies(${TYPE} ${target})
    endforeach()
endforeach()
