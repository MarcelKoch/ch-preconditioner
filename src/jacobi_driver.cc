#include "config.h"

#include <string>
#include "dune/common/parallel/mpihelper.hh"
#include "dune/common/parametertree.hh"
#include "dune/common/parametertreeparser.hh"
#include "dune/grid/yaspgrid.hh"
#include "dune/grid/io/file/vtk/subsamplingvtkwriter.hh"
#include "dune/pdelab/constraints/conforming.hh"
#include "dune/pdelab/gridoperator/blockstructured.hh"
#include "dune/pdelab/function/callableadapter.hh"
#include "dune/pdelab/common/functionutilities.hh"
#include "dune/pdelab/stationary/linearproblem.hh"
#include "dune/pdelab/gridfunctionspace/vtk.hh"
#include "dune/pdelab/gridfunctionspace/gridfunctionadapter.hh"
#include "dune/testtools/gridconstruction.hh"
#include "dune/codegen/matrixfree.hh"
#include "dune/codegen/blockstructured/blockstructuredqkfem.hh"
#include "dune/codegen/blockstructured/preconditioner/preconditioner.hh"
#include "dune/codegen/vtkpredicate.hh"

#include "jacobi_block-@NBLOCKS@_rOperator_file.hh"
#include "jacobi_block-@NBLOCKS@_rCoarseOperator_file.hh"
#include "jacobi_block-@NBLOCKS@_rPointDiagonal_file.hh"
#include "jacobi_block-@NBLOCKS@_interpolation_CG1_operator_file.hh"
#include "jacobi_block-@NBLOCKS@_restriction_BCG1_operator_file.hh"


constexpr int NBLOCKS = @NBLOCKS@;

int main(int argc, char** argv){
  // Initialize basic stuff...
  Dune::MPIHelper::instance(argc, argv);
  using RangeType = double;
  Dune::ParameterTree initree{};

  using Grid = Dune::YaspGrid<2>;
  using GV = typename Grid::LeafGridView;
  using DF = Grid::ctype;

  const unsigned int N = std::stoi(argv[1]);

  auto grid_ptr = Dune::StructuredGridFactory<Grid>::createCubeGrid(Dune::FieldVector<double, 2>{0. ,0.},
                                                                    Dune::FieldVector<double, 2>{1., 1.},
                                                                    std::array<unsigned int, 2>{N, N});
  GV gv = grid_ptr->leafGridView();
  std::cout << "N " << N << std::endl;
  std::cout << "n " << N * NBLOCKS << std::endl;

  // Set up finite element maps...
  using FEM = Dune::PDELab::BlockstructuredQkLocalFiniteElementMap<GV, DF, RangeType, NBLOCKS>;
  FEM fem(gv);

  // Set up grid function spaces...
  using VB = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
  using DirichletConstraintsAssember = Dune::PDELab::ConformingDirichletConstraints;
  using GFS = Dune::PDELab::GridFunctionSpace<GV, FEM, DirichletConstraintsAssember, VB>;
  GFS gfs(gv, fem);
  gfs.name("gfs");

  // Set up constraints container...
  using CC = GFS::ConstraintsContainer<RangeType>::Type;
  CC cc;
  cc.clear();
  auto bctype_lambda = [&](const auto& x){ return 1.0; };
  auto bctype = Dune::PDELab::makeBoundaryConditionFromCallable(gv, bctype_lambda);
  Dune::PDELab::constraints(bctype, gfs, cc);

  // Set up grid grid operators...
  using LOP = rOperator<GFS, GFS>;
  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  using GOP = Dune::PDELab::BlockstructuredGridOperator<GFS, GFS, LOP, MB, DF, RangeType, RangeType, CC, CC>;
  LOP lop(gfs, gfs, initree);
  gfs.update();
  MB mb(9);
  GOP gop(gfs, cc, gfs, cc, lop, mb);
  std::cout << "DOF " << gfs.size() << std::endl;

  // Set up finite element maps...
  using C_FEM = Dune::PDELab::BlockstructuredQkLocalFiniteElementMap<GV, DF, RangeType, 1>;
  C_FEM c_fem(gv);

  // Set up grid function spaces...
  using C_GFS = Dune::PDELab::GridFunctionSpace<GV, C_FEM, DirichletConstraintsAssember>;
  C_GFS c_gfs(gv, c_fem);
  c_gfs.name("c_gfs");

  // Set up constraints container...
  using C_CC = C_GFS::ConstraintsContainer<RangeType>::Type;
  C_CC c_cc;
  c_cc.clear();
  Dune::PDELab::constraints(bctype, c_gfs, c_cc);
  std::cout << "CDOF " << c_gfs.size() << std::endl;

  // Set up grid grid operators...
  using C_LOP = rCoarseOperator<C_GFS, C_GFS>;
  C_LOP c_lop(c_gfs, c_gfs, initree);
  c_gfs.update();

  using C_GOP = Dune::PDELab::GridOperator<C_GFS, C_GFS, C_LOP, MB, DF, RangeType, RangeType, C_CC, C_CC>;
  C_GOP c_gop(c_gfs, c_cc, c_gfs, c_cc, c_lop, mb);

  // Set up solution vectors...
  using X = Dune::PDELab::Backend::Vector<GFS,DF>;
  X x_r(gfs), z(gfs);
  x_r = 0.0;
  auto func_0000 = Dune::PDELab::makeGridFunctionFromCallable(gv, [](const auto& x){
    return x[0] * x[0] + x[1] + x[1];
  });
  Dune::PDELab::interpolate(func_0000, gfs, x_r);

  SchurDecomposition<GFS, CC> decomp(gfs, cc);
  using Decomposition = decltype(decomp);

  RestrictionBCG1LocalOperator<GFS, GFS> r_lop(gfs, gfs, initree);
  RestrictionOperator restriction(r_lop, gfs, cc, c_gfs, c_cc, decomp);
  using R = decltype(restriction);

  InterpolationCG1LocalOperator<GFS, GFS> i_lop(gfs, gfs, initree);
  InterpolationOperator interpolation(i_lop, gfs, cc, c_gfs, c_cc, decomp);
  using I = decltype(interpolation);

  rPointDiagonal<GFS, GFS> point_diagonal_lop(gfs, gfs, initree);

  JacobiPreconditioner<X, GOP, decltype(point_diagonal_lop)> pre_jacobi(gop, point_diagonal_lop);

  CoarseGridCorrection<X, I, R, C_GOP> pre_coarse(interpolation, restriction, c_gop);

  AdditiveTwoLevel pre_twoLevel(pre_jacobi, pre_coarse);

  Dune::PDELab::solveMatrixFree(gop, x_r, pre_twoLevel);
}
